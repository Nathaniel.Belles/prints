# prints

A place to store all my 3D prints. 

- [prints](#prints)
  - [AirTag Keychain](#airtag-keychain)
  - [Lonboard Mods](#lonboard-mods)
  - [Phone Holder](#phone-holder)
  - [Test Prints](#test-prints)


## AirTag Keychain
- [AirTag Keychain.stl](<AirTag Keychain/AirTag Keychain.stl>)

An AirTag holder for keychains. Super slim and compact. Have been using this since I got my first AirTag and and haven't had it break yet. 

## Lonboard Mods
- [End Cover.stl](<Lonboard Mods/End Cover.stl>)
- [Tool Holder.stl](<Lonboard Mods/Tool Holder.stl>)
- [Handle Bottom.stl](<Lonboard Mods/Handle Bottom.stl>)
- [Handle Top.stl](<Lonboard Mods/Handle Top.stl>)

Back in college, I purchased an electric longboard called the Acton Qu4dro which was a massive 4-motor aluminum electric lonboard (which seems to not be manufactured anymore according to the [website](http://acton.fun)). When I started using it I found that I needed a couple modifications to make it nice to use every day. The first is an end cover to act as a plastic bumper in case the longboard hits something or I scratch it when standing it up on one end. Included with the lonboard was a set of tools for fixing it (a couple wrenches, hex allen wrenches, and remote batteries). I created a holder for holding those tools in place inside the tool case it came with. After a while of dragging the lonboard around, I found that carrying it was a lot because it was super heavy so dragging it on it's wheels was a better way to move it around. This was hard to do because I'm relatively tall and so I created this handle that smooths out the interface with my hand while also putting the lonboard a little closer to the ground making it possible to drag it cleanly. 

## Phone Holder

- [Phone Holder.stl](<Phone Holder/Phone Holder.stl>)

I have looked around for a while for a phone holder that I can use in my car (and other various places) that doesn't require a mechanism to open/close it. I created this phone holder that is super simple and quick to print. It just holds the phone horizontally by gripping the front and back of the phone along one edge. I use it with some double sided tape to hold it in place. I do recommend printing it in something like ABS instead of PLA for the increased temperature resistance if left in the car for long periods of time (I have had one melt and bend before).

## Test Prints

- [Dimensional Adjustment 10x10.stl](<Test Prints/Dimensional Adjustment 10x10.stl>)
- [Dimensional Adjustment 20x20.stl](<Test Prints/Dimensional Adjustment 20x20.stl>)
- [Retraction.stl](<Test Prints/Retraction.stl>)

These are just a couple prints I created to help with calibrating one of my 3D printers. They are pretty self explanatory but the first two are used for checking the outer printing dimensions for specifically calibrated parts. The last is for quickly checking retraction settings between two pillars close to each other.