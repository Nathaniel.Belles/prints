// Set resolution
$fn=100;

// Check if animating to overwrite $t variable
animation = false;

// Magicband dimensions
magicband_pin_to_pin_length = 249;

// Pin dimenstions
pin_diameter = 2.5;
pin_radius = pin_diameter / 2;
pin_height = 4;
pin_to_pin_gap = 7;
pin_to_edge = 7;
nub_radius = 1.5;
nub_height = 0.5;
hole_offest = 0.2;

// Body dimensions
body_length = magicband_pin_to_pin_length + (2 * pin_to_edge);
body_width = 20;
body_height = 3;
body_rounding = 9.5;

module rounded_cube(dimensions, radius, center) {
    length = dimensions[0];
    width = dimensions[1];
    height = dimensions[2];
    linear_extrude(height, center=center) {
        offset(r=radius){
            offset(delta=-radius){
                square([length, width], center=center);
            }
        }
    }
}

// Used for animating
difference() {
    // Object ///////////////
    union(){
        // Body /////////////// 
        difference(){
            // Body
            rounded_cube([body_length, body_width, body_height], body_rounding, center=true);
            
            // Pin holes
            union(){
                translate([-((body_length / 2) - pin_to_edge), 0, 0])
                    // Holes
                    union(){
                        // Outer pin
                        union(){
                            // Upward half
                            cylinder(h=body_height, r=nub_radius + 0.05);
                            // Downward half
                            scale([1, 1, -1])
                                cylinder(h=body_height, r=nub_radius + hole_offest);
                        }
                    
                        // Inner pin
                        translate([pin_to_pin_gap, 0, 0])
                        union(){
                            // Upward half
                            cylinder(h=body_height, r=nub_radius + 0.05);
                            // Downward half
                            scale([1, 1, -1])
                                cylinder(h=body_height, r=nub_radius + hole_offest);
                        }
                    }
            }
        }
        ///////////////////////

        // Pins ///////////////
        union(){
            // Outer pin
            translate([(body_length / 2) - pin_to_edge, 0, 0])
                union(){
                    // Pin
                    cylinder(h=pin_height + (body_height / 2), r=pin_radius);
                    translate([0, 0, pin_height + (body_height / 2) - nub_height])
                        // Nub
                        cylinder(h=nub_height, r=nub_radius);
                }
            // Inner pin
            translate([(body_length / 2) - pin_to_edge - pin_to_pin_gap, 0, 0])
                union(){
                    // Pin
                    cylinder(h=pin_height + (body_height / 2), r=pin_radius);
                    translate([0, 0, pin_height + (body_height / 2) - nub_height])
                        // Nub
                        cylinder(h=nub_height, r=nub_radius);
                }
        }
        ///////////////////////
    }

    // Only enable if animating
    if (!animation) {
        $t = 1.0;
        translate([-150, -150, -body_height/2 - 0.001 + ($t * (body_height + pin_height + 0.002))])
        cube([300, 300, 20]);
    }
}